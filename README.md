# Lynda.com Tutorial - Building a Django Application

This will be a great way to further develop my python skills

## 1. Setting Up a Django Project

### Initiate Project
Use $ django-admin.py startproject [projectName] to create project with Django

django-admin.py -> boilerplate content
startproject -> initiates the project
[projectName] -> whatever you want to call the project

### Investigate Project template files

manage.py -> Use this to run commands (DO NOT EDIT)

shelter/__init__.py -> Tells Python that this folder contains Python files (DO NOT EDIT)

shelter/swgi.py -> Provides hook for web servers (i.e. such as Apache) (DO NOT EDIT)

shelter/settings.py -> This configures Django (EDIT)

shelter/urls.py -> routes requests based on URL (EDIT)

### Run the project for the first time

1) Cd into root
$ cd wisdompets/
2) Run Server
$ python3 manage.py runserver
3) Navigate to browser and type
localhost:8000

### Create Django App

~ An app is a folder with Python files within Django...The app is a component
~ The app refers to a set of features (i.e. Blog, Pet Adoption, etc.)

1) Start app
$ python3 manage.py startapp [nameOfApp]

manage.py -> runs commands
startapp -> this creates our app
[nameOfApp] -> this provides a title for our app

2) Navigate to the project directory [wisdompets] and open the settings.py file
~ Scroll to the INSTALLED_APPS section and add the [nameOfApp] to the lis

INSTALLED_APPS = [...,'adoptions']

Anatomy of App

apps.py -> Configuration and initialization (Controls settings specific to this app)
models.py -> Data layer (To construct data schema and data layers)
admin.py -> Administrative interface (to see and edit data related to the app)
urls.py -> URL routing
views.py -> Control layer (Handles HTTPs responses)
tests.py -> Tests the app
migrations/ -> Holds migration files (Used to help migrate database as schema changes over time)

## 2. Working with Django Models and the Admin.

* Django uses the Model, View, Controller architecture
(Elements)
a) URL Patterns -> urls.py (URLs specify view to display)
b) Views -> views.py (Views reference template and model to display information)
c) Templates -> templates
d) Models -> models.py

Ex. http://yoursite.com/

/ -> home -> adoptions/index.html

Ex. http://yoursite.com/adoptions/123/

/adoptions/#/ -> pet_detail -> adoptions/pet_detail.html


### (Django Models)

~ Models create the data layer of an app (i.e. defines the database structure)
~ Use models to query the database
~ A model is a *class* inheriting from django.db.models.Model, and is used to define fields as class attributes
~ Models may be viewed as a spreadsheet, where each field is a column of the model

(Requirements)
1) store pets with a name, age, vaccination records and other details

### (Django Fields(

Ex. Creating a Model

from django.db import models

class Item(models.Model):
	title = models.CharField(max_length=200)
	description = models.TextField()
	amount = models.IntegerField()

~ Field Types
Numerical
1) IntegerField - -1,0,1
2) DecimalField - 0.5, 3.14

Textual
1) CharField - "Product Name"
2) TextField - "To ele..."
3) EmailField - test@email.com
4) URLField - www.test.com

MISC
1) BooleanField - True/False
2) DateTimeField - datetime(1960,1,1,8,0,0)

Relational Data
1) ForeignKey -> Relates single db key from one model to another
2) ManyToMany -> Relates many records to many

Field Attribute Options
1) max_length
2) null -> If True can be blank
3) blank -> If True this is optional
4) default ->
5) choices -> Limits the options for the field

***Tips***
https://www.djangoproject.com

### (Implement models and fields)

Path:
~ wisdompets> adoptions> models.py

Purpose:
~ Create model to store pet info. that inherits from models.Model

Actions:
~ Create (2) models that store pet information and vaccines
~ Link the models with a ManyToManyField() method by passing in the model name of second as a parameter.



### (Django migrations)

Definitions:
1) Models -> Define the structure of database tables
2) Migrations -> Generate scripts to change the database structure as we update our code and change our models

Purpose:
* The initial migration creates the database tables post a new model being defined
* Migration required when: Adding models, adding fields, removing fields or changing fields

Migration commands:
1) python3 manage.py makemigrations -> Creates migration files and inspects the current state of the database to id what changes need to be made to ensure the database structure matches the models file.  These files start at 1 and are placed in the app/ migrations director
2) python3 manage.py showmigrations -> Runs all migrations that have not yet been ran.

a) (Option to run specific migration) -> migrate <appname> <number>

*Caution* -> Unapplied Migrations
a) When migrations are created and not ran...Common source of erros in development.
b) Know who is working on models and communicate code changes.

3) python3 manage.py migrate

Process:
1) Create migration
$ python3 manage.py makemigrations
2) Review files
$ ls adoptions/migrations
3) Observe which migrations exist
$ python3 manage.py showmigrations
[ ] -> This empty square bracket suggests the migrations have not been applied
4) Run migrations
$ python3 manage.py migrate
5) Run show migrations to verify results
$ python3 manage.py showmigrations
[x] -> This square bracket with 'x' suggests the migration has ran

Side:
1) Review the structure of the DB -> use http://sqlitebrowser.org
2) ID is automatically created with Django models and is usually the primary key

### (Import CSV data)

Purpose:
~ Load data into model from csv file

Process:
1) Copy csv file into same directory with manage.py
2) Copy over management directory with commands to adoption folder, which includes the load pet data file
3) Review load_pet_data.py file to see how command will load csv file
4) Load Data > Navigate to manage.py directory
$ python3 manage.py load_pet_data
5) Confirm data loaded via SQLBrowser

### (Work with Django admin)

Purpose:
~ Create admin. panel for project

Path:
~ wisdompets> adoptions> admin.py>

Process:
1) Import Pet model
2) Create interface for Pet Model
3) Create superuser via terminal -> navigate to manage.py directory
$ python3 manage.py createsuperuser

a) create user name and password
4) Run server
$python3 manage.py runserver
5) Update admin.py file to display list of attributes
class PetAdmin(admin.ModelAdmin):
6) Update the models.py to override the generic object
def __str__(self):
		return self.name

### (Query data with the Django ORM)

Process:
1) Open interactive python shell
$ python3 manage.py shell
2) Import Pet model
from adoptions.models import Pet
3) Select all records in the model
Pet.objects.all()
4) assign object to variable
pets = Pet.objects.all()
5) Select single record
pet = pets[0]
6) Review instance
pet.name
7) Retrieve single instance
-> id references the record row
$ pet = Pet.objects.get(id=1)

~ (Caution) this method may return data does not exist or query returns more than one result.
~ (Solution) we need to account for this
8) Review relational connections
$ pet = Pet.objects.get(id=7)
$ pet.vaccinations.all()
