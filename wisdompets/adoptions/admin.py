from django.contrib import admin

# Import the Pet model

from .models import Pet

# Register the Class with the Admin model
@admin.register(Pet)

# Create Admin Interface
class PetAdmin(admin.ModelAdmin):
	"""
	"""
	# pass

	# Use this to display name instead of just object within admin panel

	list_display = ['name', 'species', 'breed', 'age', 'sex']
