from django.db import models

# Create model that inherits models.Model

class Pet(models.Model):
	
	"""
	Create model to store pet information

	Parameters:
	models.Model - Parent Class

	Attributes:
	name:  name of pet with character field with max length of 100
	submitter: who rescued pet with character field with max length of 100
	species: pet species with character field with max length of 30
	breed: pet breed that is option with character field with max length of 30
	description: describes the pet with open text field
	sex: sex field limited to only male or female that is optional
	submission_date: datetimefield to record the rescue date
	age: age of pet as integer that can be N/A...Blank could be confused with 0.
	vaccinations: this is the link between the (2) models and is many to many
	b/c a vaccine could be given to many pets

	"""

	SEX_CHOICES = [('M', 'Male'), ('F','Female')]
	name = models.CharField(max_length=100)
	submitter = models.CharField(max_length=100)
	species = models.CharField(max_length=30)
	breed = models.CharField(max_length=30, blank=True)
	description = models.TextField()
	sex = models.CharField(choices=SEX_CHOICES, max_length=1, blank=True)
	submission_date = models.DateTimeField()
	age = models.IntegerField(null=True)
	vaccinations = models.ManyToManyField('Vaccine', blank=True)


# Create model class for tracking Vaccines

class Vaccine(models.Model):
	
	"""
	Create model to store vaccine information

	Parameters:
	models.Model - Parent Class

	Attributes:
	name:  name of vaccine with character field with max length of 100	
	"""

	name =  models.CharField(max_length=50)

	# Use this to override the generic object definition of vaccines

	def __str__(self):
		return self.name


# Create link between the two models...Since vaccines can be give to many pets
# this is a many to many link

