from django.shortcuts import render
from django.http import HttpResponse
from django.http import Http404

# Import models to display pet info.

from .models import Pet


# Implement View functions below
def home(request):
	pets = Pet.objects.all()
	return render(request, 'home.html', {'pet': pets})


def pet_detail(request, id):
	try:
		pet = Pet.objects.get(id=id)
	except Pet.DoesNotExist:
		raise Http404('Pet not found')
	return render(request, 'pet_detail.html', {'pet': pet})

